<?php

namespace App\Http\Controllers\Admin\Section;

use App\Http\Controllers\Controller;
use App\Http\Requests\Section\ManageSectionRequest;
use App\Repositories\SectionRepository;
use DataTables;

/**
 * Class SectionTableController.
 */
class SectionTableController extends Controller
{	
	/**
	 * @var $sectionRepository
	 */
	protected $sectionRepository;

	/**
	 * SectionTableController constructor.
	 *
	 * @param SectionRepository $sectionRepository
	 */
	public function __construct(SectionRepository $sectionRepository)
	{
		$this->sectionRepository = $sectionRepository;
	}

	/**
	 * @param ManageSectionRequest $request
	 *
	 * @return mixed
	 */
	public function __invoke(ManageSectionRequest $request)
	{

		return Datatables::of($this->sectionRepository->getForDataTable())
			->addColumn('index_column', function ($section){
				return '';
			})
			->addColumn('active', function ($section){
				return $section->status;
			})
			->addColumn('actions', function ($section){
				return $section->action_buttons;
			})
			->rawColumns(['active', 'actions'])
			->make(true);
	}

}
