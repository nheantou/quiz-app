<?php

namespace App\Http\Controllers\Admin\Section;

use App\Http\Controllers\Controller;
use App\Http\Requests\Section\ManageSectionRequest;
use App\Http\Requests\Section\UpdateSectionRequest;
use App\Repositories\SectionRepository;
use App\Models\Section\Section;

class SectionController extends Controller
{

	/**
	 * @var SectionRepository
	 */
	protected $sectionRepository;

	/**
	 * SectionController constructor.
	 *
	 * @param SectionRepository $sectionRepository
	 */
	public function __construct(SectionRepository $sectionRepository)
	{
		$this->sectionRepository = $sectionRepository;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('admin.sections.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('admin.sections.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(ManageSectionRequest $request)
	{
		$this->sectionRepository->create($request->only(
			'title',
			'active'
		));

		return redirect()->route('admin.section.index')->withFlashSuccess(__('alerts.sections.created'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Section $section)
	{
		return view('admin.sections.edit')
			->withSection($section);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(UpdateSectionRequest $request, Section $section)
	{
		$this->sectionRepository->update($section, $request->only(
			'title',
			'active'
		));

		return redirect()->route('admin.section.index')->withFlashSuccess(__('alerts.sections.updated'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
