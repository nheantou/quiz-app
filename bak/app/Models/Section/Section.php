<?php

namespace App\Models\Section;

use Illuminate\Database\Eloquent\Model;
use App\Models\Section\Traits\Attribute\SectionAttribute;

class Section extends Model
{
	use SectionAttribute;
    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'title',
		'active'
	];

	protected $dates = [
		'created_at',
		'updated_at',
	];
}
