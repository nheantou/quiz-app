<?php

namespace App\Models\Section\Traits\Attribute;

use Illuminate\Support\Facades\Hash;

/**
 * Trait SectionAttribute.
 */
trait SectionAttribute
{

	/**
	 * @return string
	 */
	public function getEditButtonAttribute()
	{
		return '<a href="' . route('admin.section.edit', $this) . '" class="btn btn-success"><i class="fas fa-edit"></i></a>';
	}
	/**
	 * @return string
	 */
	public function getDeleteButtonAttribute()
	{
		return '<a href="' . route('admin.section.delete', $this) . '" class="btn btn-danger"><i class="fas fa-trash"></i></a>';
	}

	/**
	 * @return string
	 */
	public function getStatusAttribute()
	{
		return ($this->active ? '<i class="text-success fa fa-check fa-lg"></i>' : '<i class="text-danger fa fas fa-times fa-lg"></i>');
	}

	/**
	 * @return string
	 */
	public function getActionButtonsAttribute()
	{
		return '
			<div class="btn-group" role="group">
				' . $this->edit_button . $this->delete_button . '
			</div>';
	}
}
