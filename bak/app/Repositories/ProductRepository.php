<?php

namespace App\Repositories;

use App\Models\Product\Product;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class ProductRepository.
 */
class ProductRepository extends BaseRepository
{
	/**
	 * @return string
	 */
	public function model()
	{
		return Product::class;
	}

	/**
	 * @param array $data
	 *
	 * @return Participant
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function create(array $data) : Product
	{
		return DB::transaction(function () use ($data) {
			$product = parent::create([
				'name'			=> $data['name'],
				'category_id'	=> $data['category'],
				'description'	=> $data['description'],
				'price'			=> $data['price'],
			]);

			if ($product) {
				return $product;
			}
			
			throw new GeneralException(__('exceptions.provinces.create_error'));
		});
	}

	/**
	 * @param Province  $province
	 * @param array $data
	 *
	 * @return Province
	 * @throws GeneralException
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function update(Province $province, array $data) : Province
	{
		return DB::transaction(function () use ($province, $data) {
			if ($province->update([
				'name' => $data['name'],
				'city' => $data['city']
			])) {

				return $province;
			}

			throw new GeneralException(__('exceptions.provinces.update_error'));
		});
	}
}
