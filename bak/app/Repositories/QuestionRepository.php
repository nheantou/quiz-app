<?php

namespace App\Repositories;

use App\Models\Question\Question;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class SectionRepository.
 */
class QuestionRepository extends BaseRepository
{
	/**
	 * @return string
	 */
	public function model()
	{
		return Question::class;
	}

	/**
	* @return mixed
	*/
	public function getForDataTable()
	{
		return $this->model->select(['id', 'title', 'active', 'created_at', 'updated_at']);
    }

	/**
	 * @param array $data
	 *
	 * @return Section
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function create(array $data) : Section
	{
		return DB::transaction(function () use ($data) {
			$section = parent::create([
				'title'				=> $data['title'],
				'active'			=> (isset($data['active']) ? 1 : 0),
			]);

			if ($section) {
				return $section;
			}

			throw new GeneralException(__('exceptions.section.create_error'));
		});
	}

	/**
	 * @param Section  $section
	 * @param array $data
	 *
	 * @return Section
	 * @throws GeneralException
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function update(Question $question, array $data) : Question
	{
		return DB::transaction(function () use ($question, $data) {
			if ($question->update([
				'title'				=> $data['title'],
				'active'			=> (isset($data['active']) ? 1 : 0),
			])) {

				return $question;
			}

			throw new GeneralException(__('exceptions.section.update_error'));
		});
	}

}
