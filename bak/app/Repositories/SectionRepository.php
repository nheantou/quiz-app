<?php

namespace App\Repositories;

use App\Models\Section\Section;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class SectionRepository.
 */
class SectionRepository extends BaseRepository
{
	/**
	 * @return string
	 */
	public function model()
	{
		return Section::class;
	}

	/**
	* @return mixed
	*/
	public function getForDataTable()
	{
		return $this->model->select(['id', 'title', 'active', 'created_at', 'updated_at']);
    }

	/**
	 * @param array $data
	 *
	 * @return Section
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function create(array $data) : Section
	{
		return DB::transaction(function () use ($data) {
			$section = parent::create([
				'title'				=> $data['txtsection'],
				'active'			=> (isset($data['checkebox']) ? 1 : 0),
			]);

			if ($section) {
				return $section;
			}

			throw new GeneralException(__('exceptions.section.create_error'));
		});
	}

	/**
	 * @param Section  $section
	 * @param array $data
	 *
	 * @return Section
	 * @throws GeneralException
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function update(Section $section, array $data) : Section
	{
		return DB::transaction(function () use ($section, $data) {
			if ($section->update([
				'title'				=> $data['title'],
				'active'			=> (isset($data['active']) ? 1 : 0),
			])) {

				return $section;
			}

			throw new GeneralException(__('exceptions.section.update_error'));
		});
	}

}
