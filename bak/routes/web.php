<?php
Route::redirect('/', '/login');

Route::redirect('/home', '/admin');

Auth::routes(['register' => false]);

//Admin
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.', 'middleware' => ['auth']], function () {

	//Section
	Route::group(['namespace' => 'Section', 'prefix' => 'section', 'as' => 'section.'], function () {
		/*
		 * For DataTables
		 */
		Route::post('get', 'SectionTableController')->name('get');

		/*
		 * CRUD
		 */
		Route::get('/', 'SectionController@index')->name('index');
		Route::get('create', 'SectionController@create')->name('create');
		Route::post('/', 'SectionController@store')->name('store');				

		/*
		 * Specified
		 */
		Route::group(['prefix' => '{section}'], function () {
			Route::get('/edit', 'SectionController@edit')->name('edit');
			Route::patch('/', 'SectionController@update')->name('update');
			Route::delete('/', 'SectionController@delete')->name('delete');
		});
	});
});

//User
Route::group(['middleware' => ['auth']], function () {
	Route::get('/', 'HomeController@index')->name('home');
	
	//Quiz
	Route::group(['prefix' => 'quiz', 'as' => 'quiz.'], function () {
		Route::get('/', 'QuizController@index')->name('index');		
		Route::get('/result', 'QuizController@result')->name('result');
	});
});

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
	Route::get('/', 'HomeController@index')->name('home');

	Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');

	Route::resource('permissions', 'PermissionsController');

	Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');

	Route::resource('roles', 'RolesController');

	Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');

	Route::resource('users', 'UsersController');

	Route::delete('products/destroy', 'ProductsController@massDestroy')->name('products.massDestroy');

	Route::resource('products', 'ProductsController');
});
