<?php

Breadcrumbs::for('admin.home', function ($trail) {
	$trail->push(__('strings.dashboard.title'), route('admin.home'));
});

//User
Breadcrumbs::for('admin.users.index', function ($trail) {
	$trail->parent('admin.home');
	$trail->push(__('strings.user.title'), route('admin.users.index'));
});
Breadcrumbs::for('admin.users.create', function ($trail) {
	$trail->parent('admin.home');
	$trail->push(__('strings.user.title'), route('admin.users.create'));
});

//Section
Breadcrumbs::for('admin.section.index', function ($trail) {
	$trail->parent('admin.home');
	$trail->push(__('strings.section.title'), route('admin.section.index'));
});
Breadcrumbs::for('admin.section.create', function ($trail) {
	$trail->parent('admin.section.index');
	$trail->push(__('strings.section.title'), route('admin.section.create'));
});
Breadcrumbs::for('admin.section.edit', function ($trail, $id) {
	$trail->parent('admin.section.index');
	$trail->push(__('strings.section.title'), route('admin.section.edit', $id));
});

//Question
Breadcrumbs::for('admin.question.index', function ($trail) {
	$trail->parent('admin.home');
	$trail->push(__('strings.question.text'), route('admin.question.index'));
});
Breadcrumbs::for('admin.question.create', function ($trail) {
	$trail->parent('admin.question.index');
	$trail->push(__('strings.question.text'), route('admin.question.create'));
});
Breadcrumbs::for('admin.question.edit', function ($trail, $id) {
	$trail->parent('admin.question.index');
	$trail->push(__('strings.question.text'), route('admin.question.edit', $id));
});

//Student
Breadcrumbs::for('admin.student.index', function ($trail) {
	$trail->parent('admin.home');
	$trail->push(__('strings.student.text'), route('admin.student.index'));
});
Breadcrumbs::for('admin.student.create', function ($trail) {
	$trail->parent('admin.student.index');
	$trail->push(__('strings.student.text'), route('admin.student.create'));
});
Breadcrumbs::for('admin.student.edit', function ($trail, $id) {
	$trail->parent('admin.student.index');
	$trail->push(__('strings.student.text'), route('admin.student.edit', $id));
});

//Role
Breadcrumbs::for('admin.roles.index', function ($trail) {
	$trail->parent('admin.home');
	$trail->push(__('strings.roles.title'), route('admin.roles.index'));
});

//Home
Breadcrumbs::for('home', function ($trail) {	
	$trail->push(__('strings.home.title'), route('home'));
});

