<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Alert Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain alert messages for various scenarios
	| during CRUD operations. You are free to modify these language lines
	| according to your application's requirements.
	|
	*/

	'sections' => [
		'created' => 'ទិន្នន័យត្រូវបានរក្សាទុកដោយជោគជ័យ.',
		'updated' => 'ប្រលងតេស្តត្រូវបានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ.',
	],
];
