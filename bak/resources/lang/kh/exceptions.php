<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Exception Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used in Exceptions thrown throughout the system.
	| Regardless where it is placed, a button can be listed here so it is easily
	| found in a intuitive way.
	|
	*/

	'sections' => [
		'create_error'		=> 'មានបញ្ហាក្នុងការបង្កើត ការប្រលង។ សូម​ព្យាយាម​ម្តង​ទៀត។',
		'update_error'		=> 'មានបញ្ហាក្នុងការធ្វើបច្ចុប្បន្នភាព ការប្រលង។ សូម​ព្យាយាម​ម្តង​ទៀត។',
	],
];
