<?php

return [
	'open'			=> 'បើក',
	'close'			=> 'បិទ',

	'dashboard' => [
		'title' => 'ផ្ទាំងគ្រប់គ្រង'
	],

	'sections' => [
		'title'		=> 'គ្រប់គ្រងការប្រលង',
		'create'	=> 'បង្កើត',

		'table'		=> [
			'title'		=> 'ការប្រលងតេស្ត',
			'active'	=> 'ស្ថានភាព',
			'action'	=> 'សកម្មភាព',
		],
	],

	'copyright' => 'រក្សាសិទ្ធគ្រប់យ៉ាងដោយ សាលាបើកបរ អមតៈ',
	'generation' => 'ប្រព័ន្ធនេះបង្កើតដោយ ដេលី សូលូសិន(Daily Solution). កំណែ ១.០'
];
