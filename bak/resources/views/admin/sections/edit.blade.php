@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('admin.section.update', ['section' => $section]) }}">
	@csrf
	@method('PATCH')
	<div class="card">
		<div class="card-header">
			<h4>{{ trans('strings.sections.title') }}<small class="text-muted"> 
			| {{ trans('strings.edit') }}</small></h4>
		</div>

		<div class="card-body">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="title">{{ __('validation.attributes.title') }}</label>
						<input class="form-control" name="title" id="title" value="{{ $section->title }}" type="text" placeholder="{{ __('validation.attributes.title') }}">
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="title" class="d-block">{{ __('validation.attributes.active') }}</label>
						
						<div class="d-flex">
							<label class="m-2">{{ __('strings.close') }}</label>
							<label class="switch switch-pill switch-lg switch-success">
								<input type="checkbox" name="active" class="switch-input" {{ ($section->active ? "checked" : "") }}>
								<span class="switch-slider"></span>
							</label>
							<label class="m-2">{{ __('strings.open') }}</label>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="card-footer">
			<div class="row">
				<div class="col text-right">
					<a class="btn btn-danger" href="{{ route('admin.section.index') }}"><i class="fas fa-ban"></i> {{ __('buttons.cancel') }}</a>
					<button type="submit" class="btn btn-success"><i class="fas fa-save"></i> {{ __('buttons.crud.save') }}</button>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection