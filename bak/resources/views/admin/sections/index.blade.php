@extends('layouts.admin')

@push('before-styles')
	<link href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-5">
				<h4 class="card-title mb-0">
					{{ __('labels.letters.management') }} <small class="text-muted">{{ __('labels.letters.local') }}</small>
				</h4>
			</div>

			<div class="col-sm-7">
				<div class="btn-toolbar float-right" role="toolbar" aria-label="@lang('labels.general.toolbar_btn_groups')">
					<a href="{{ route('admin.section.create') }}" class="btn btn-success btn-min ml-1"><i class="fas fa-plus-circle"></i> @lang('buttons.crud.create_new')</a>
				</div>
			</div>
		</div>

		<div class="mt-4">
			<table class="table table-bordered table-striped table-hover dt-responsive nowrap" id="master-table">
				<thead>
					<tr class="text-center">
						<th class="numberic">#</th>
						<th>{{ __('strings.sections.table.title') }}</th>
						<th>{{ __('strings.sections.table.active') }}</th>
						<th>{{ __('strings.sections.table.action') }}</th>
					</tr>
				</thead>
				<tbody class="text-center"></tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@push('scripts')
	<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

	<script>
		var _url = "{{ route('admin.section.get') }}";
		var _columns = [
			{data: 'index_column', name: 'index_column', searchable: false, sortable: false, class: 'text-center'},
			{data: 'title'},
			{data: 'active', searchable: false, sortable: false, class: 'text-center'},
			{data: 'actions', searchable: false, sortable: false, class: 'text-center'}
		];

		$(function () {
			
			// var _columns = [
			// 	{data: 'index_column', name: 'index_column', searchable: false, sortable: false, class: 'text-center'}
			// ];

			// var _mission = $('#mission-table').DataTable({
			// 	processing: true,
			// 	searching: true,
			// 	serverSide: true,
			// 	pageLength: 25,
			// 	ajax: {
			// 		url: '{{ route("admin.section.get") }}',
			// 		type: "GET",
			// 	},
			// 	// language: {
			// 	// 	url: '{{ asset('plugins/datatables/i18n/kh.lang') }}',
			// 	// },
			// 	columns: _columns,
			// 	order: [[ 1, 'asc' ]],
			// });
		});
	</script>
@endpush