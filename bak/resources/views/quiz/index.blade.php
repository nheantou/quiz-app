@extends('layouts.app')

@section('content')
<style>
    /* @keyframes click-wave {
  0% {
    height: 40px;
    width: 40px;
    opacity: 0.35;
    position: relative;
  }
  100% {
    height: 200px;
    width: 200px;
    margin-left: -80px;
    margin-top: -80px;
    opacity: 0;
  }
} */
    img{
      width: 100px;
    }
    .imgQuestion{
        padding: 5px;
        width: 50%;

    }
    .text-right{
      float: right;
    }
    .question-text{
      font-size:20px;
      padding: 10px;
      max-height: 200px;
    }

    .option-input {
        -webkit-appearance: none;
        -moz-appearance: none;
        -ms-appearance: none;
        -o-appearance: none;
        appearance: none;
        position: relative;
        top: 13.33333px;
        right: 0;
        bottom: 0;
        left: 0;
        height: 40px;
        width: 40px;
        transition: all 0.15s ease-out 0s;
        background: #cbd1d8;
        border: none;
        color: #fff;
        cursor: pointer;
        display: inline-block;
        margin-right: 0.5rem;
        outline: none;
        position: relative;
        z-index: 1000;
}
.option-input:hover {
  background: #9faab7;
}
.option-input:checked {
  background: #20a8d8;
}
.option-input:checked::before {
  height: 40px;
  width: 40px;
  position: absolute;
  content: '✔';
  display: inline-block;
  font-size: 26.66667px;
  text-align: center;
  line-height: 40px;
}
.option-input:checked::after {
  -webkit-animation: click-wave 0.65s;
  -moz-animation: click-wave 0.65s;
  animation: click-wave 0.65s;
  background: #20a8d8;
  content: '';
  display: block;
  position: relative;
  z-index: 100;
}
.option-input.radio {
  border-radius: 50%;
}
.option-input.radio::after {
  border-radius: 50%;
}
.checkAnswer {
  margin-top: 0px;
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: box;
  color: #9faab7;
  font-family: "Helvetica Neue", "Helvetica", "Roboto", "Arial", sans-serif;
  text-align: center;
}
.checkAnswer div {
}
.checkAnswer label {
  display: block;
  line-height: 40px;
  font-size: 20px;
  color: black;
}
</style>
<div class="container">
<div class="row">
  <div class="col-sm-2 text-right">
    <img src="img/user.png">
  </div>
  <div class="col-sm-6">
    <p>ការប្រឡងតេស្ត#​ ០០១</p>
    <p>អត្តលេខអ្នកប្រើប្រាស់# <span>០០៧</span></p>
    <p>ឈ្មោះអ្នកប្រើប្រាស់#  <span>ទិត</span></p>

  </div>

  <div class="col-sm-4  text-right">
    <p>ម៉ោងចាប់ផ្តើម៖ <span>2:00 PM</span></p>
    <p>ម៉ោងបញ្ចប់៖ <span>3:00 PM</span></p>
    <p>រយះពេល៖ <span>60 MIN</span></p>
  </div>
</div>
<div class="row">
<div class="col-sm-12">
<div class="card">
  <div class="card-header">សំណួរ<span class="text-right">សំណួរទី 1/40</span></div>
  <div class="card-body">
      <div class="row">
      <div class="col-sm-6">
    <div class="question-text"><span>1.</span>
     ​រថយន្ត​មួយ​កំពុង​ធ្វើ​ចរាចរណ៍​នៅ​លើ​ផ្លូវជាតិ​ ​និង​មួយ​ទៀត​ចេញ​ពី​ផ្លូវ​លំ ​ ​តើ​រថយន្ត​មួយ​ណា​មាន​សិទ្ធិ​អាទិភាព?
    </div>
    <div class="question-text"><img class="imgQuestion" src="img/upload/q1.jpg"></div>
    </div>
    <div class="col-sm-6">
    <div class="question-text">ចម្លើយ៖</div>
    <div class="checkAnswer">
<div>
  <label>
    <input type="radio" class="option-input radio" name="example" />
    ​រថយន្ត​កំពុង​ធ្វើ​ចរាចរ​លើ​ផ្លូវ​ជាតិ​មាន​សិទ្ធិ​អាទិភាព
  </label>
  <label>
    <input type="radio" class="option-input radio" name="example" />
    ​រថយន្ត​កំពុង​ធ្វើ​ចរាចរ​លើ​ផ្លូវ​ជាតិ​មាន​សិទ្ធិ​អាទិភាព
  </label>
  <label>
    <input type="radio" class="option-input radio" name="example" />
    ​រថយន្ត​កំពុង​ធ្វើ​ចរាចរ​លើ​ផ្លូវ​ជាតិ​មាន​សិទ្ធិ​អាទិភាព
  </label>
  <label>
    <input type="radio" class="option-input radio" name="example" />
    ​រថយន្ត​កំពុង​ធ្វើ​ចរាចរ​លើ​ផ្លូវ​ជាតិ​មាន​សិទ្ធិ​អាទិភាព
  </label>
</div>
</div>
    </div>
  </div>
</div>
  <div class="card-footer">
    <button class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i> ទៅក្រោយវិញ</button>
    <span class="text-right">
    <button class="btn btn-primary btn-sm"> ទៅសំណួរបន្ទាប់ <i class="fa fa-arrow-right"></i></button>
    </span>

  </div>
</div>
</div>
</div>
@endsection

@push('scripts')

@endpush
