@extends('layouts.app')

@section('content')
<style>
    .container{
      //background: cornflowerblue;
    }
    img{
      width: 100px;
    }
    .imgQuestion{
        padding: 5px;
        width: 80%;
    }
    .text-right{
      float: right;
    }
    .question-text{
      font-size:20px;
      padding: 10px;
    }


</style>
<div class="container">
<div class="row">
  <div class="col-sm-2 text-right">
    <img src="../img/user.png">
  </div>
  <div class="col-sm-6">
    <p>ការប្រឡងតេស្ត#​ ០០១</p>
    <p>អត្តលេខអ្នកប្រើប្រាស់# <span>០០៧</span></p>
    <p>ឈ្មោះអ្នកប្រើប្រាស់#  <span>ទិត</span></p>

  </div>

  <div class="col-sm-4  text-right">
    <p>ម៉ោងចាប់ផ្តើម៖ <span>2:00 PM</span></p>
    <p>ម៉ោងបញ្ចប់៖ <span>3:00 PM</span></p>
    <p>រយះពេល៖ <span>60 MIN</span></p>
  </div>
</div>
<div class="row">
<div class="col-sm-12">
<div class="card">
  <div class="card-header"><div style="text-align: center;">អបអរសាទរអ្នកជាប់ការប្រឡង</div></div>
  <div class="card-body">
      <div class="row">
      <div class="col-sm-6">
        <div style="text-align: center;">
          <div><h1>ពន្ទុសរុប</h1></div>
          <div><h2>៤០</h2></div>
        </div>
    </div>
    <div class="col-sm-6">
      <div style="text-align: center;">
          <div><h1>មធ្យមភាគសរុប</h1></div>
          <div><h2>១០០</h2></div>
      </div>
  </div>
  <hr style="height: 2px;background-color: black;width: 90%;">
    <div class="col-sm-12">
      <div><h1>លទ្ធផល: ជាប់!</h1></div>
    </div>
  </div>
</div>
  <div class="card-footer">
      <div style="text-align: center;">អរគុណសម្រាប់ការប្រឡង!</div>
  </div>
</div>
</div>
</div>
@endsection

@push('scripts')

@endpush
