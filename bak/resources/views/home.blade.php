@extends('layouts.app')

@section('content')
<style>
  .container{
    margin-top: 20px;
  }
  img{
    width: 150px;
  }
  .logo{
    text-align: center;

  }
  .logo,h2{
    font-size: 20px;
  }
  .btn-lg{
    padding: 20px;
    margin: 20px 0px;
  }
  .header-menu{
    padding: 20px;
    background: #20a8d8;
    font-size: 20px;
    color: white;
    text-align: center;
  }
  </style>
<div class="content">
<div class="container">
<div class="row">
<div class="col-sm-12">
  <div class="logo">
  <img src="img/logo.png">
  </div>
</div>
</div>
<div class="card">
    <div class="card-header header-menu">កម្មវិធីបើកបរ</div>
<div class="card-body">
<div class="row">
  <div class="col-sm-6">
  <button id="btnExam" type="button" id="btnLogin" class="btn btn-primary btn-lg btn-block">ចូលតេស្តប្រឡង</button>
  </div>
  <div class="col-sm-6">
  <button type="button" id="btnResult" class="btn btn-primary btn-lg btn-block">មើលលទ្ធផល</button>
  </div>
</div>
</div>
</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $('#btnExam').click(function(){
    window.location.href = "/quiz";
  });
  $('#btnResult').click(function(){
    window.location.href="resultpage.php";
  })
});
</script>
@endsection

@push('scripts')


@endpush
