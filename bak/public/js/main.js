$(document).ready(function () {
	window._token = $('meta[name="csrf-token"]').attr('content');

	__ajaxSetup();
	var _table = $('#master-table').DataTable({
		processing: true,
		searching: true,
		serverSide: true,
		pageLength: 25,
		language: {
			url: _lang,
		},
		ajax: {
			url: _url,
			type: "POST",
		},
		columns: _columns,
		order: [[ 1, 'asc' ]],
	});

	_table.on('draw.dt', function () {
		var info = _table.page.info();
		_table.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1 + info.start;
		});
	});

	// ClassicEditor.create(document.querySelector('.ckeditor'))

	// moment.updateLocale('en', {
	//   week: {dow: 1} // Monday is the first day of the week
	// })

	// $('.date').datetimepicker({
	//   format: 'YYYY-MM-DD',
	//   locale: 'en'
	// })

	// $('.datetime').datetimepicker({
	//   format: 'YYYY-MM-DD HH:mm:ss',
	//   locale: 'en',
	//   sideBySide: true
	// })

	// $('.timepicker').datetimepicker({
	//   format: 'HH:mm:ss'
	// })

	// $('.select-all').click(function () {
	//   let $select2 = $(this).parent().siblings('.select2')
	//   $select2.find('option').prop('selected', 'selected')
	//   $select2.trigger('change')
	// })
	// $('.deselect-all').click(function () {
	//   let $select2 = $(this).parent().siblings('.select2')
	//   $select2.find('option').prop('selected', '')
	//   $select2.trigger('change')
	// })

	// $('.select2').select2()

	// $('.treeview').each(function () {
	//   var shouldExpand = false
	//   $(this).find('li').each(function () {
	//     if ($(this).hasClass('active')) {
	//       shouldExpand = true
	//     }
	//   })
	//   if (shouldExpand) {
	//     $(this).addClass('active')
	//   }
	// })
})

function __ajaxSetup() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
}
