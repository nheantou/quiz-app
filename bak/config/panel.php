<?php

return [
    'date_format'         => 'Y-m-d',
    'time_format'         => 'H:i:s',
    'primary_language'    => 'kh',
    'available_languages' => [
        'kh' => 'ភាសាខ្មែរ',
        'en' => 'English',
    ],
];
