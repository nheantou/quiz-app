var khmer = ['ក', 'ខ', 'គ', 'ឃ', 'ង','ច', 'ឆ', 'ជ', 'ឈ', 'ញ'];
var Result = [];
var Questionlength =0;
var QuestionID = 0;
var Result1;
var AnswerID = 0;
function checkAnswer(){
    $.ajax({
        type:'POST',
        url:'model/ResultModel.php',
        data:{question_id:QuestionID,answer_id:AnswerID},
        success:function(response){
           if(response[0]['correct_answer']>0){
           Result1 = 1;
           }else{
           Result1 = 0;
           }
           return Result1;
        }
    });
}
function showquestion(index){
    $.ajax({
        type: 'POST',
        url: 'model/QuizModel.php',
        data: { question: 10 },
        success: function(response) {
        //Count Questions
        Questionlength = response.length;
        //Set Header Text
        $('.queston-legth').html(Questionlength);
        $('.text-question').html(index+1+". "+response[index]['question_text']);
        //Set Image Question
        $('#question-img').attr('src', 'upload/'+response[index]['question_image']);
        QuestionID = response[index]['id'];
        //Start Get Answer by QuestionID
          $.ajax({
              type:'POST',
              url:'model/QuizModel.php',
              data: { question_id: response[index]['id'] },
          success:function(response){
              $.each(response,function(index){
                 $('#myLabel'+index).html(khmer[index]+". "+response[index]['answer_text']);
                 $('#myRadio'+index).val(response[index]['id']);
                 $('#myRadio'+index).prop( "checked", false );
              });
          }
          })
      }
      });
}
$(document).ready(function() {
    var index = 0;
    var isChecked = false;
    var Result = [];
    showquestion(index);

    $('#btnNext').click(function(){
        //Protect CheckBox
        if(isChecked){
            index+=1;
        //Check Correct Answer
        $.ajax({
            type:'POST',
            url:'model/ResultModel.php',
            data:{question_id:QuestionID,answer_id:AnswerID},
            success:function(response){
               if(response[0]['correct_answer']>0){
               Result.push(1);
               }else{
               Result.push(0);
               }
               return Result1;
            }
        });
        //console.log(Result);
        if(index<Questionlength){
            showquestion(index);
            $('.question-id').html(index+1);
        }else{
            console.log(Result);
            $.ajax({
                type:'POST',
                url:'view/homepage/result.php',
                data:{result:Result},
                success:function(response){
                    $('#mycard-header').html('Your Result');
                    $('#mycard').html(response);
                    $('#btnNext').attr('href','index.php')
                    $('#btnNext').html("Try Again <i class='fas fa-undo-alt'></i>");
                }
            });
            
        }
        isChecked = false;
    }else{
        alert("Please Give Me the Answer!");
    }
    });
    //Protect Not Answer
    $('input:radio').each(function(index){
        $(this).click(function(){
           AnswerID = $('#myRadio'+index).val();
           isChecked = true;
        });
      });

  });