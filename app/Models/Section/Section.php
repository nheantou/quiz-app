<?php

namespace App\Models\Section;

use App\Models\Question\Question;
use Illuminate\Database\Eloquent\Model;
use App\Models\Section\Traits\Relationship\SectionRelationship;
use App\Models\Section\Traits\Attribute\SectionAttribute;

/**
 * Class Section.
 */
class Section extends Model
{
	use SectionRelationship,
		SectionAttribute;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'title',
		'active',
		'during',
		'time'
	];

	/**
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at'
	];

	public function questions()
	{
		return $this->hasMany(Question::class);
	}
}
