<?php

namespace App\Models\Section\Traits\Relationship;

use App\Models\Question\Question;
use App\Models\AnswerUser\AnswerUser;
use App\Models\Answer\Answer;

/**
 * Class SectionRelationship.
 */
trait SectionRelationship
{
	/**
	 * @return mixed
	 */
	public function questions()
	{
		return $this->hasMany(Question::class);
	}

	/**
	 * @return mixed
	 */
	public function answerUser()
	{
		return $this->hasMany(AnswerUser::class);
	}

	/**
	 * @return mixed
	 */
	public function scoreUser($user)
	{
		return $this->belongsToMany(Answer::class, 'answers_users', 'section_id', 'answer_id')
			->where('user_id', $user)
			->where('correct', 1);
	}


}
