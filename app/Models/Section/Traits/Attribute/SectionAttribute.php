<?php

namespace App\Models\Section\Traits\Attribute;

/**
 * Trait SectionAttribute.
 */
trait SectionAttribute
{
    /**
	 * @return string
	 */
	public function getResultAttribute()
	{
		if(($this->total - $this->score) >= 5) {
			return __('strings.quiz.pass');
		}

		return __('strings.quiz.fail');
    }

    	/**
	 * @return string
	 */
	public function getShowButtonAttribute()
	{
		return '<a class="btn btn-success mx-1" href="' . route('admin.section.show', $this->id) . '"><i class="fa icon-magnifier-add"></i></a>';
    }
    public function getEditButtonAttribute(){
        return '<button type="button" data-remote="' . route('admin.section.edit', $this->id) . '" class="btnEdit btn btn-info mx-1">
        <i class="fa icon-pencil"></i>
        </button>';
    }
    public function getDeleteButtonAttribute(){
        return '<button type="button" data-remote="' . route('admin.section.destroy', $this->id) . '" class="btnDelete btn btn-danger mx-1">
        <i class="fa icon-trash"></i>
        </button>';
    }
    /**
	 * @return string
	 */
	public function getActionButtonsAttribute()
	{

		return $this->show_button . $this->edit_button . $this->delete_button;
	}
}
