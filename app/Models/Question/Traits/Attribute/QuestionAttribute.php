<?php

namespace App\Models\Question\Traits\Attribute;

use Illuminate\Support\Facades\Hash;

/**
 * Trait QuestionAttribute.
 */
trait QuestionAttribute
{
	/**
	 * @return string
	 */
	public function getFinishedButtonAttribute()
	{
		return  '<a id="finish" href="' . route('frontend.quiz.result', $this->section_id) . '" class="btn btn-sm btn-warning float-right">' . __('buttons.finished') . '</a>';
	}

	/**
	 * @return string
	 */
	public function getPrevNextAttribute()
	{
		$prev = '<button class="btn btn-danger btn-sm" disabled><i class="fa fa-arrow-left"></i> ' . __('buttons.questions.prev') . '</button>';
		if ($this->order > 1) {
			$prev = '<a href="#" id="prev" class="btn btn-danger btn-sm btnPN"><i class="fa fa-arrow-left"></i> ' . __('buttons.questions.prev') . '</a>';
		}

		$caption = __('buttons.questions.next');
		if ($this->order == $this->total) {
			$caption = __('buttons.questions.last');
		}
		$next = '<a href="#" id="next" class="btn btn-primary btn-sm btnPN float-right">' . $caption . ' <i class="fa fa-arrow-right"></i></a>';

		return $prev . $next;
	}
	/**
	 * @return string
	 */
	public function getShowButtonAttribute()
	{
		return '<a class="btn btn-success mx-1" href="' . route('admin.section.show', $this->id) . '"><i class="fa icon-magnifier-add"></i></a>';
    }
    public function getEditButtonAttribute(){
        return '<button type="button" data-remote="' . route('admin.section.edit', $this->id) . '" class="btnEdit btn btn-info mx-1">
        <i class="fa icon-pencil"></i>
        </button>';
    }
    public function getDeleteButtonAttribute(){
        return '<button type="button" data-remote="' . route('admin.section.destroy', $this->id) . '" class="btnDelete btn btn-danger mx-1">
        <i class="fa icon-trash"></i>
        </button>';
    }
    /**
	 * @return string
	 */
	public function getActionButtonsAttribute()
	{

		return $this->show_button . $this->edit_button . $this->delete_button;
	}
}
