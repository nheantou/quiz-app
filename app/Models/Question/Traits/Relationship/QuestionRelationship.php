<?php

namespace App\Models\Question\Traits\Relationship;

use App\Models\Section\Section;
use App\Models\Answer\Answer;
use App\Models\AnswerUser\AnswerUser;
use Auth;

/**
 * Class QuestionRelationship.
 */
trait QuestionRelationship
{
	/**
	 * @return mixed
	 */
	public function section()
	{
		return $this->belongsTo(Section::class);
	}

	/**
	 * @return mixed
	 */
	public function answers()
	{
		return $this->hasMany(Answer::class);
	}

	/**
	 * @return mixed
	 */
	public function answerUser()
	{
		return $this->hasOne(AnswerUser::class)->where('user_id', Auth::user()->id);
	}
}
