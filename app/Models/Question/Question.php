<?php

namespace App\Models\Question;

use Illuminate\Database\Eloquent\Model;
use App\Models\Question\Traits\Relationship\QuestionRelationship;
use App\Models\Question\Traits\Attribute\QuestionAttribute;
use App\Models\Section\Section;

/**
 * Class Question.
 */
class Question extends Model
{
	use QuestionRelationship,
		QuestionAttribute;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'text',
		'section_id',
		'active',
		'order',
		'image',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'active' => 'boolean',
	];

	/**
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at'
	];

	/**
	 * @return bool
	 */
	public function isActive()
	{
		return $this->active;
	}

	public function scopeActive($query)
	{
		return $query->where('active', 1);
	}
	public function section(){
		return $this->belongsTo(Section::class);
	}
}
