<?php

namespace App\Models\Answer;

use Illuminate\Database\Eloquent\Model;
use App\Models\Answer\Traits\Relationship\AnswerRelationship;

/**
 * Class Answer
 */
class Answer extends Model
{
	use AnswerRelationship;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'question_id',
		'text',
		'image',
		'correct',
	];

	/**
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at'
	];
}
