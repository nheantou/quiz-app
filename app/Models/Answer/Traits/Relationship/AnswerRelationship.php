<?php

namespace App\Models\Answer\Traits\Relationship;

use App\Models\Auth\User;

/**
 * Class AnswerRelationship
 */
trait AnswerRelationship
{
	/**
	 * @return mixed
	 */

	public function user()
	{
		return $this->belongsToMany(User::class);
	}
}
