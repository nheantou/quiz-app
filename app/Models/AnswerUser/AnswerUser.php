<?php

namespace App\Models\AnswerUser;

use Illuminate\Database\Eloquent\Model;
use App\Models\AnswerUser\Traits\Relationship\AnswerUserRelationship;

class AnswerUser extends Model
{
	use AnswerUserRelationship;

    protected $table = 'answers_users';

    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'answer_id',
		'user_id',
		'question_id',
		'section_id',
	];

	/**
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at'
	];
}
