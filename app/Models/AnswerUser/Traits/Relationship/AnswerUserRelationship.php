<?php

namespace App\Models\AnswerUser\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Answer\Answer;
use App\Models\Question\Question;
use App\Models\Section\Section;

/**
 * Class AnswerUserRelationship.
 */
trait AnswerUserRelationship
{
	/**
	 * @return mixed
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * @return mixed
	 */
	public function answer()
	{
		return $this->belongsTo(Answer::class);
	}

	/**
	 * @return mixed
	 */
	public function countAnswers()
	{
		return $this->hasMany(Answer::class);
	}

	/**
	 * @return mixed
	 */
	public function question()
	{
		return $this->belongsTo(Question::class);
	}

	/**
	 * @return mixed
	 */
	public function section()
	{
		return $this->belongsTo(Section::class);
	}
}
