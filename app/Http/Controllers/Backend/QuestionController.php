<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Question\Question;
use Illuminate\Http\Request;
use App\Repositories\QuestionRepository;
use App\Models\Section\Section;
class QuestionController extends Controller
{
    	/*
	 * SectionController constructor.
	 *
	 * @param QuestionRepository $QuestionRepository
	 */

	public function __construct(QuestionRepository $questionRepository)
	{
		$this->questionRepository = $questionRepository;
    }

    public function index()
    {
        $questions = Question::all();
        return view('backend.question.index',compact('questions'));
    }
    public function create()
    {
        $sections = Section::all();
        return view('backend.question.create', compact('sections'));
    }

    public function save(Request $request){
        $request->validate([
            'correct' => 'required',
        ]);
        $question =  $this->questionRepository->create($request->only(
            'txtquestion',
            'section',
            'fileImage',
            'answers',
            'correct'
        ));
        if(isset($question)){
            return redirect('admin/question/');
        }
    }

    public function store(Request $request, Section $section){
        $request->merge(['section_id' => $section->id]);
        $question =  $this->questionRepository->create($request->only(
            'txtquestion',
            'section_id',
            'fileImage',
            'answers',
            'correct'
        ));
        if(isset($question)){
            return redirect('admin/section/'.$request->section_id);
        }
    }
}
