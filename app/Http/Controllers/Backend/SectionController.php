<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Section\Section;
use App\Repositories\SectionRepository;

class SectionController extends Controller
{
    	/*
	 * SectionController constructor.
	 *
	 * @param SectionRepository $sectionRepository
	 */

	public function __construct(SectionRepository $sectionRepository)
	{
		$this->sectionRepository = $sectionRepository;
    }

    public function index()
    {
        $sections = Section::all();
        return view('backend.section.index',compact('sections'));
    }

    public function show(Section $section){
        return view('backend.section.question',compact('section'));
    }

    public function store(Request $request){
        if(isset($request->active)){
            Section::where('active', 1)->update(['active' => 0]);
        }
        $this->sectionRepository->create($request->only(
            'title',
            'active',
            'during',
        ));
        return redirect('admin/section');
    }

    public function delete($id){
        
    }

    public function edit($id){
        $section = Section::find($id);
        return response()->JSON($section);
    }
}
