<?php

namespace App\Http\Controllers\Backend;

use App\Models\Auth\User;
use App\Http\Controllers\Controller;
use App\Events\Backend\Auth\User\UserDeleted;
use App\Repositories\Backend\Auth\RoleRepository;
use App\Repositories\Backend\Auth\UserRepository;
use App\Repositories\Backend\Auth\PermissionRepository;
use App\Http\Requests\Backend\Auth\User\StoreUserRequest;
use App\Http\Requests\Backend\Auth\User\ManageUserRequest;
use App\Http\Requests\Backend\Auth\User\UpdateUserRequest;
use App\Models\Section\Section;
use Request;

class StudentController extends Controller
{
    protected $userRepository;
    
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index(ManageUserRequest $request)
    {
        return view('backend.student.index')
            ->withData($this->userRepository->getActivePaginated(25, 'id', 'asc'));
    }
    public function create()
    {
        dd("Create");
    }

    public function save(Request $request){
        $request->validate([
            'correct' => 'required',
        ]);
        $question =  $this->questionRepository->create($request->only(
            'txtquestion',
            'section',
            'fileImage',
            'answers',
            'correct'
        ));
        if(isset($question)){
            return redirect('admin/question/');
        }
    }

    public function store(Request $request, Section $section){
        $request->merge(['section_id' => $section->id]);
        $question =  $this->questionRepository->create($request->only(
            'txtquestion',
            'section_id',
            'fileImage',
            'answers',
            'correct'
        ));
        if(isset($question)){
            return redirect('admin/section/'.$request->section_id);
        }
    }
}
