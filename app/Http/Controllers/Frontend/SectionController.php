<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Section\Section;
use App\Repositories\Frontend\SectionRepository;
use App\Repositories\AnswerUserRepository;

class SectionController extends Controller
{
	/**
	 * @var SectionRepository
	 */
	protected $sectionRepository;

	/*
	 * SectionController constructor.
	 *
	 * @param SectionRepository $sectionRepository
	 */
	 
	public function __construct(SectionRepository $sectionRepository)
	{
		$this->sectionRepository = $sectionRepository;
	}

	/**
	 * Display a section of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		// get only active section
		$section = $this->sectionRepository->findActivedSection();

		// redirect if empty section
		if (empty($section)) {
			return redirect()->route(home_route())->withFlashDanger(__('No section active'));;
		}
		return view('frontend.quiz.index')
			->withSection($section);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Section $section, AnswerUserRepository $answerUserRepository)
	{
		//print_r($section);
		// set deactived to section when time finished
		// $this->sectionRepository->mark($section);

		// total questions
		$section->total = $section->questions->where('active', 1)->count();
		//dd($section);
		// get user score
		$section->score = $section->scoreUser(auth()->user()->id)->count();
		//dd($section);
		return view('frontend.sections.result')
			->withSection($section);
	}
}
