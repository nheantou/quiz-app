<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Question\Question;
use App\Repositories\QuestionRepository;
use App\Repositories\Frontend\AnswerRepository;
use App\Repositories\AnswerUserRepository;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
	/**
	 * @var QuestionRepository
	 */
	protected $questionRepository;

	/*
	 * QuestionController constructor.
	 *
	 * @param QuestionRepository $questionRepository
	 */

	public function __construct(QuestionRepository $questionRepository)
	{
		$this->questionRepository = $questionRepository;
	}

	/**
	 * Load first question by section id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($secId, Request $request, AnswerUserRepository $answerUserRepository)
	{
		// save user answer, except first loading or answer is 0
		if (!empty($request->question) && !empty($request->answer)) {
			$request->merge(['section' => $secId]);
			$answerUserRepository->create($request->only([
				'section',
				'question',
				'answer'
			]));
		}

		$total = 0;
		$question = $this->questionRepository->getQuestionByOrder($secId, $request->order);

		if ($question) {
			// get total question in the same section
			$total = $this->questionRepository->getTotal($secId);
			$question->total = $total;
		}

		if ($request->order > $total) {
			return response()->json(['finished' => 1], 200);
		}

		$view = view('frontend.quiz.show')
			->withQuestion($question)
			->render();

		return response()->json(['finished' => 0, 'question' => $question->id, 'html' => $view], 200);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	// public function show(Question $question, Request $request, AnswerUserRepository $answerUserRepository)
	// {
	// 	//save user answer, except question prev is 0
	// 	if($question->prev != 0 && $request->answer != 0) {
	// 		$request->merge(['question' => $question->prev, 'section' => $question->section_id]);
	// 		$answer = $answerUserRepository->create($request->only(['answer', 'question', 'section']));
	// 	}

	// 	$view = '';
	// 	if($question->isActive()) {
	// 		//get total question in the same section
	// 		$question->total = $this->questionRepository->getTotal($question->section_id);

	// 		$view = view('frontend.quiz.show')
	// 			->withQuestion($question)
	// 			->render();
	// 	}

	// 	//redirect if after last question
	// 	if($question->total < $question->order) {
	// 		$link = route('frontend.quiz.result', 1);
	// 	}

	// 	return response()->json(['html' => $view, 'link' => isset($link) ? $link : ''], 200);
	// }
}
