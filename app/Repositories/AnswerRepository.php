<?php

namespace App\Repositories;

use App\Models\Answer\Answer;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class AnswerRepository.
 */
class AnswerRepository extends BaseRepository
{
	/**
	 * @return string
	 */
	public function model()
	{
		return Answer::class;
	}

	/**
	* @return mixed
	*/
	public function getForDataTable()
	{
		return $this->model->select(['id', 'que_text', 'image', 'active', 'created_at', 'updated_at']);
    }

	/**
	 * @param array $data
	 *
	 * @return Answer
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function create(array $data) : Answer
	{
		return DB::transaction(function () use ($data) {
			$answer = parent::create([
                'ans_text'			    => $data['section_id'],
                'correct'			    => $data['section_id'],
			]);

			if ($answer) {
				return $answer;
			}

			throw new GeneralException(__('exceptions.Answer.create_error'));
		});
    }
    public function findLastAnswer(){
        return Answer::orderBy('id', 'desc')->first();
    }

	/**
	 * @param Answer  $answer
	 * @param array $data
	 *
	 * @return Answer
	 * @throws GeneralException
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function update(Answer $answer, array $data) : Answer
	{
		return DB::transaction(function () use ($answer, $data) {
			if ($answer->update([
				'title'				=> $data['title'],
				'active'			=> (isset($data['active']) ? 1 : 0),
			])) {
				return $answer;
			}

			throw new GeneralException(__('exceptions.answer.update_error'));
		});
	}

}
