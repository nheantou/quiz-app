<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Models\AnswerUser\AnswerUser;
use Auth;

/**
 * Class AnswerUserRepository.
 */
class AnswerUserRepository extends BaseRepository
{
	/**
	 * @return string
	 */
	public function model()
	{
		return AnswerUser::class;
	}

	/**
	 * @param array $data
	 *
	 * @throws \Exception
	 * @throws \Throwable
	 * @return \Illuminate\Database\Eloquent\Model|mixed
	 */
	public function create(array $data): AnswerUser
	{
		return DB::transaction(function () use ($data) {
			$answerUser = $this->model->updateOrCreate([
				'question_id'	=> $data['question'],
				'user_id'		=> auth()->user()->id
			], [
				'answer_id'		=> $data['answer'],
				'user_id'		=> auth()->user()->id,
				'question_id'	=> $data['question'],
				'section_id'	=> $data['section'],
			]);

			if ($answerUser) {
				return $answerUser;
			}

			//throw new GeneralException(__('exceptions.answer.create_error'));
		});
	}
}
