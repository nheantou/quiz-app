<?php

namespace App\Repositories;

use App\Models\Question\Question;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Answer\Answer;

/**
 * Class QuestionRepository.
 */
class QuestionRepository extends BaseRepository
{
	/**
	 * @return string
	 */
	public function model()
	{
		return Question::class;
	}

	/**
	* @return mixed
	*/
	public function getForDataTable()
	{
		return $this->model->select(['id', 'text', 'image', 'active', 'created_at', 'updated_at']);
	}

	/**
	* @return mixed
	*/
	public function getQuestionByOrder($secId=0, $order=1)
	{
		return $this->model
			->where('section_id', $secId)
			->where('order', $order)
			->where('active', 1)
			->first();
	}

	/**
	 * @return string
	 */
	public function getTotal($secId)
	{
		return $this->model
			->where('section_id', $secId)
			->active()
			->count();
	}

	/**
	 * @param array $data
	 *
	 * @return Question
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function create(array $data) : Question
	{
		return DB::transaction(function () use ($data) {
			if(isset($data['fileImage'])){
				$imageName = time().'.'.$data['fileImage']->getClientOriginalExtension();
				$data['fileImage']->move(public_path('img/upload/'.$data['section']), $imageName);
			}
			else {
				$imageName = null;
			}

			$order = Question::where('section_id',$data['section'])->orderBy('order', 'desc')->first();

			$question = parent::create([
				'text'					=> $data['txtquestion'],
				'section_id'			=> $data['section'],
				'order'					=> isset($order) ? $order->order + 1 : 1 ,
				'image'			        => $imageName,
				'active'			    => 1,
			]);

			if ($question) {

				if (!isset($data['answers']) || ! count($data['answers'])) {
					$data['answers'] = [];
				}

				$answers = array();
				foreach ($data['answers'] as $key => $value) {
					$answers[] = new Answer([
					'text'  => $value,
					'correct'  => ($key == $data['correct'] ? 1 : 0),
					]);
				}
				$question->answers()->saveMany($answers);

				return $question;
			}

			throw new GeneralException(__('exceptions.question.create_error'));
		});
	}


	/**
	 * @param Question  $Question
	 * @param array $data
	 *
	 * @return Question
	 * @throws GeneralException
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function update(Question $question, array $data) : Question
	{
		return DB::transaction(function () use ($question, $data) {
			if ($question->update([
				'title'				=> $data['title'],
				'active'			=> (isset($data['active']) ? 1 : 0),
			])) {
				return $question;
			}

			throw new GeneralException(__('exceptions.Question.update_error'));
		});
	}

}
