<?php

namespace App\Repositories\Frontend;

use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Models\Answer\Answer;
use Auth;
/**
 * Class AnswerRepository.
 */
class AnswerRepository extends BaseRepository
{
	/**
	 * @return string
	 */
	public function model()
	{
		return Answer::class;
	}

	/**
	 * @return string
	 */
	public function findFirst($secId)
	{
		return $this->model
			->where('section_id', $secId)
			->active()
			->orderBy('order')
			->first();
	}

	/**
	 * @param array $data
	 *
	 * @throws \Exception
	 * @throws \Throwable
	 * @return \Illuminate\Database\Eloquent\Model|mixed
	 */
	public function create(array $data)
	{
		return DB::transaction(function () use ($data) {
			$answer = $this->model->where('id', $data['answer'])->first();

			// $user = parent::create([
			// 	'first_name' => $data['first_name'],
			// 	'last_name' => $data['last_name'],
			// 	'email' => $data['email'],
			// 	'confirmation_code' => md5(uniqid(mt_rand(), true)),
			// 	'active' => true,
			// 	'password' => $data['password'],
			// 	// If users require approval or needs to confirm email
			// 	'confirmed' => ! (config('access.users.requires_approval') || config('access.users.confirm_email')),
			// ]);

			if ($answer) {
				//$answer->user()->associate();
				$answer->user()->sync(Auth::user()->id);
				return $answer;
			}

			// // Return the user object
			// return $user;
		});
	}
}
