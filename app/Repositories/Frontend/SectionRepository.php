<?php

namespace App\Repositories\Frontend;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Storage;
use App\Models\Section\Section;

/**
 * Class SectionRepository.
 */
class SectionRepository extends BaseRepository
{
	/**
	 * @return string
	 */
	public function model()
	{
		return Section::class;
	}

	public function findActivedSection()
	{
		return $this->model
			->where('active', 1)
			->first();
	}
}
