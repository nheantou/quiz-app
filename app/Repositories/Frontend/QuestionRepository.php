<?php

namespace App\Repositories\Frontend;

use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Models\Question\Question;

/**
 * Class QuestionRepository.
 */
class QuestionRepository extends BaseRepository
{
	/**
	 * @return string
	 */
	public function model()
	{
		return Question::class;
	}

	/**
	 * @return string
	 */
	public function findFirst($secId)
	{
		return $this->model
			->where('section_id', $secId)
			->active()
			->orderBy('order')
			->first();
	}

	/**
	 * @return string
	 */
	public function getTotal($secId)
	{
		return $this->model
			->where('section_id', $secId)
			->active()
			->count();
	}
}
