<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('section_id')->unsigned();
            $table->integer('question_id')->unsigned();
            $table->integer('answer_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
        // Schema::table('answers_users', function (Blueprint $table) {
        //     //rest of fields then...
        //     $table->foreign('section_id')->references('id')->on('sections')->onDelete('cascade');
        //     $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
        //     $table->foreign('answer_id')->references('id')->on('answers')->onDelete('cascade');
        //     $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers_users');
    }
}
