<?php
use App\Http\Controllers\Backend\SectionController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\QuestionController;
use App\Http\Controllers\Backend\StudentController;
// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

// Section
Route::group(['prefix' => 'section', 'as' => 'section.'], function () {
    Route::get('/', [SectionController::class, 'index'])->name('index');
    Route::post('/', [SectionController::class, 'store'])->name('store');
    Route::get('{section}', [SectionController::class, 'show'])->name('show');
    Route::post('{id}/edit', [SectionController::class, 'edit'])->name('edit');
    Route::post('{id}/delete', [SectionController::class, 'delete'])->name('destroy');
});
// Question
Route::group(['prefix' => 'question', 'as' => 'question.'], function () {
    Route::get('/', [QuestionController::class, 'index'])->name('index');
    Route::post('/', [QuestionController::class, 'store'])->name('store');
    Route::post('/', [QuestionController::class, 'save'])->name('save');
    Route::get('/create', [QuestionController::class, 'create'])->name('create');
    //Route::get('{question}', [QuestionController::class, 'show'])->name('show');
    Route::post('{id}/edit', [QuestionController::class, 'edit'])->name('edit');
    Route::post('{id}/delete', [QuestionController::class, 'delete'])->name('destroy');
});
//Question
Route::group(['prefix' => '{section}/question', 'as' => 'question.'], function () {
    Route::post('/', [QuestionController::class,'store'])->name('store');
});

// Student
Route::group(['prefix' => 'student', 'as' => 'student.'], function () {
    Route::get('/', [StudentController::class, 'index'])->name('index');
    Route::post('/', [StudentController::class, 'store'])->name('store');
    Route::get('{student}', [studentController::class, 'show'])->name('show');
    Route::post('{id}/edit', [studentController::class, 'edit'])->name('edit');
    Route::post('{id}/delete', [studentController::class, 'delete'])->name('destroy');
});
