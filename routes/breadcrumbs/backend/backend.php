<?php
//breadcrumbs for backend
Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

Breadcrumbs::for('admin.section.index', function ($trail) {
    $trail->push(__('Section'), route('admin.section.index'));
});
Breadcrumbs::for('admin.section.show', function ($trail, $section) {
    $trail->parent('admin.dashboard');
    $trail->push(__('Section / ' . $section->title), route('admin.section.show', $section->title));
});
Breadcrumbs::for('admin.section.createquestion', function ($trail, $section) {
    $trail->push(__('Section / ' . 1), route('admin.section.createquestion', 1));
});
Breadcrumbs::for('admin.question', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.question'));
});

//Question
Breadcrumbs::for('admin.question.index', function ($trail) {
    $trail->push(__('Question'), route('admin.question.index'));
});
Breadcrumbs::for('admin.question.create', function ($trail) {
    $trail->parent('admin.question.index');
    $trail->push(__('Create'), route('admin.question.create'));
});

//Student
Breadcrumbs::for('admin.student.index', function ($trail) {
    $trail->push(__('Student'), route('admin.question.index'));
});

Breadcrumbs::for('admin.student.create', function ($trail) {
    $trail->parent('admin.student.index');
    $trail->push(__('Create'), route('admin.student.index'));
});

require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';
