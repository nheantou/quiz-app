<?php

use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\ProfileController;
use App\Http\Controllers\Frontend\User\DashboardController;
use App\Http\Controllers\Frontend\SectionController;
use App\Http\Controllers\Frontend\QuestionController;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
// Route::get('/', function(){
// 	return redirect()->route('frontend.auth.login');
// })->name('index');
// Route::get('/', function(){
// 	return redirect()->route('frontend.auth.login');
// })->name('index');

Route::get('contact', [ContactController::class, 'index'])->name('contact');
Route::post('contact/send', [ContactController::class, 'send'])->name('contact.send');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth', 'password_expires']], function () {
	Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
		// User Dashboard Specific
		Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

		// User Account Specific
		Route::get('account', [AccountController::class, 'index'])->name('account');

		// User Profile Specific
		Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');
	});
});

Route::group(['middleware' => ['auth', 'role:user']], function () {
	Route::get('/', [HomeController::class, 'index'])->name('index');
	
	//Quiz
	Route::group(['prefix' => 'quiz', 'as' => 'quiz.'], function () {

		//Section
		Route::get('/', [SectionController::class, 'index'])->name('index');

		//load first question by section id
		Route::get('{secId}/question', [QuestionController::class, 'show'])->name('show');

		//get question
		//Route::post('{question}', [QuestionController::class, 'show'])->name('show');
		//Route::get('{question}', [QuestionController::class, 'show'])->name('show');

		//save user's answer
		//Route::post('{}', [QuestionController::class, 'store'])->name('store');

		//show result
		Route::get('{section}/result', [SectionController::class, 'show'])->name('result');	
	});
	
});
