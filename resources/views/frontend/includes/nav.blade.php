<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
	<div class="container" style="margin-top: 0px;">
		<a class="navbar-brand" href="{{ url('/') }}">
			{{ config('app.name', 'Laravel') }}
		</a>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto">
				@guest
					<li class="nav-item">
						<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
					</li>
					@if (Route::has('register'))
						<li class="nav-item">
							<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
						</li>
					@endif
				@else
					<a href="{{ route('frontend.auth.logout') }}" class="btn btn-danger btn-sm"><i class="fa fa-power-off"></i> @lang('buttons.exit')</a>
				@endguest
			</ul>
		</div>
	</div>
</nav>
