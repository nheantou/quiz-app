@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<div class="row">
	<div class="col-sm-2 text-right">
		<img class="img-fluid" src="{{ asset('img/user.png') }}">
	</div>
	<div class="col-sm-6">
		<p>ការប្រឡងតេស្ត#​ ០០១</p>
		<p>អត្តលេខអ្នកប្រើប្រាស់# <span>០០៧</span></p>
		<p>ឈ្មោះអ្នកប្រើប្រាស់#  <span>ទិត</span></p>
	</div>

	<div class="col-sm-4  text-right">
		<p>ម៉ោងចាប់ផ្តើម៖ <span>2:00 PM</span></p>
		<p>ម៉ោងបញ្ចប់៖ <span>3:00 PM</span></p>
		<p>រយះពេល៖ <span>60 MIN</span></p>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="card"></div>
	</div>
</div>
@endsection

@push('after-scripts')
<script type="text/javascript">
	$(function() {
		
	});
</script>
@endpush
