@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<div class="row">
	<div class="col-sm-2 text-right">
		<img class="img-fluid" src="{{ asset('img/user.png') }}">
	</div>
	<div class="col-sm-6">
		<p>ការប្រឡងតេស្ត#​ ០០១</p>
		<p>អត្តលេខអ្នកប្រើប្រាស់# <span>០០៧</span></p>
		<p>ឈ្មោះអ្នកប្រើប្រាស់#  <span>ទិត</span></p>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="card card-accent-success">
			<div class="card-header text-center">
				{{ ($section->result​ == __('strings.quiz.pass') ? __('strings.quiz.greeting_pass') : __('strings.quiz.greeting_fail')) }}
			</div>
			
			<div class="card-body">
				<div class="row text-center">
					<div class="col-sm-6">
						<h4>ពិន្ទុដែលទទួលបាន</h4>
						<h1>{{ $section->score }}</h1>
					</div>
					<div class="col-sm-6">
						<h4>ពិន្ទុសរុប</h4>
						<h1>{{ $section->total }}</h1>
					</div>
				</div>				
				<hr class="break-line">

				{{-- get result from section attribute --}}
				<h1 class="text-center">{{ $section->result }}</h1>

				<div class="row justify-content-md-center">
					<div class="col-lg-4">
						<div id="test-circle"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('after-scripts')
<script type="text/javascript" src="https://rawgit.com/pguso/jquery-plugin-circliful/1.0.2/js/jquery.circliful.js"></script>
<script type="text/javascript">
	$(function() {
		$("#test-circle").circliful({
			animation: 1,
			animationStep: 5,
			foregroundBorderWidth: 15,
			backgroundBorderWidth: 15,
			replacePercentageByText: {{ $section->score }} + '/' + {{ $section->total }},
			percent: (({{ $section->score }} * 100) / {{ $section->total }}),
			textSize: 28,
			textStyle: 'font-size: 12px;',
			textColor: '#666',
		});
	});
</script>
@endpush


