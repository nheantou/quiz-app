@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<div class="row justify-content-sm-center">
	<div class="col-sm-3">
		<div class="logo text-center mb-2">
			<img class="img-fluid" src="{{ asset('img/logo.png') }}">
		</div>
	</div>
</div>

<div class="card">
	<div class="card-header header-menu"> @lang('buttons.test')</div>
	<div class="card-body">
		<div class="row">
			<div class="col-sm-6">
				<a href="{{ route('frontend.quiz.index') }}" class="btn btn-primary btn-lg btn-block">@lang('buttons.test')</a>
			</div>
			<div class="col-sm-6">
				<a href="{{ route('frontend.quiz.result', 1) }}" class="btn btn-primary btn-lg btn-block">@lang('buttons.result')</a>
			</div>
		</div>
	</div>
</div>
@endsection
