@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<div class="row">
	<div class="col-sm-2 text-right">
		<img class="img-fluid" src="{{ asset('img/user.png') }}">
	</div>
	<div class="col-sm-6">
		<p>ការប្រឡងតេស្ត#​ ០០១</p>
		<p>អត្តលេខអ្នកប្រើប្រាស់# <span>០០៧</span></p>
		<p>ឈ្មោះអ្នកប្រើប្រាស់#  <span>ទិត</span></p>
	</div>

	<div class="col-sm-4  text-right">
		<p>ម៉ោងចាប់ផ្តើម៖ <span>2:00 PM</span></p>
		<p>ម៉ោងបញ្ចប់៖ <span>3:00 PM</span></p>
		<p>រយះពេល៖ <span>60 MIN</span></p>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-header text-center">
				អបអរសាទរអ្នកជាប់ការប្រឡង
			</div>
			
			<div class="card-body">
				<div class="row text-center">
					<div class="col-sm-6">
						<h1>ពន្ទុសរុប</h1>
						<h2>៤០</h2>
					</div>
					<div class="col-sm-6">
						<h1>មធ្យមភាគសរុប</h1>
						<h2>១០០</h2>
					</div>
				</div>				
				<hr class="break-line">

				<div class="row">
					<div class="col-sm-12">
						<h1>លទ្ធផល: ជាប់!</h1>
					</div>
				</div>
			</div>
		
			<div class="card-footer text-center">
				អរគុណសម្រាប់ការប្រឡង!
			</div>
		</div>
	</div>
</div>
@endsection