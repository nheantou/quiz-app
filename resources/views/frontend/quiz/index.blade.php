@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@push('after-styles')
	{{ style(asset('plugins/downCount/downcount.css')) }}
@endpush

@section('content')
	<div class="row">
		<div class="col-2 text-right">
			<img class="img-fluid" src="{{ asset('img/user.png') }}">
		</div>
		<div class="col-3">
			<p>ការប្រឡងតេស្ត៖ {{ $section->title }}</p>
			<p>អត្តលេខអ្នកប្រើប្រាស់៖ <span>{{ Auth::user()->id }}</span></p>
			<p>ឈ្មោះអ្នកប្រើប្រាស់៖  <span>{{ Auth::user()->full_name }}</span></p>
		</div>
		<div class="col-3">
			<ul id="countStartExam" class="countdown">
				<li>
					<span class="minutes">00</span>
					<p class="minutes_ref">minutes</p>
				</li>
				<li class="seperator">:</li>
				<li>
					<span class="seconds">00</span>
					<p class="seconds_ref">seconds</p>
				</li>
			</ul>
		</div>
		<div class="col-sm-4 text-right">
			<div id="beforeEx">
				<p>ម៉ោងចាប់ផ្តើម៖ <span>00:00</span></p>
				<p>ម៉ោងបញ្ចប់៖ <span>00:00</span></p>
				<p>រយះពេល៖ <span>40 នាទី</span></p>
			</div>

			<ul id="doEx" class="countdown" style="display: none;">
				<li>
					<span class="minutes">00</span>
					<p class="minutes_ref">minutes</p>
				</li>
				<li class="seperator">:</li>
				<li>
					<span class="seconds">00</span>
					<p class="seconds_ref">seconds</p>
				</li>
			</ul>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card minHeight370" id="questionContent"></div>
		</div>
	</div>

	<div id="loading" class="d-none">
		<div class="d-block text-center py-164">
			<div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
				<span class="sr-only">Loading...</span>
			</div>
		</div>
	</div>
@endsection

@push('after-scripts')
{!! script(asset('plugins/downCount/downcount.js')) !!}
<script type="text/javascript">
	$(function() {
		var _answer = _question = 0;
		var _order = 1; {{-- first question --}}
		{{-- count down to start examination --}}
		$('#countStartExam').downCount({
			date:'12/18/2019 00:56:00',
			offset: +7
		}, function () {
			__loadQuestion();
			__beforeEx();
		});		

		{{-- get next or prev question --}}
		$(document).on('click', '#next', function(e) {
			e.preventDefault();

			_order = _order + 1;
			__loadQuestion();
		});

		$(document).on('click', '#prev', function(e) {
			e.preventDefault();
			
			_order = _order - 1;
			__loadQuestion();
		});

		$(document).on('click', 'input[name="answer"]', function() {
			_answer = $(this).val();
		});

		$(document).on('click', '#finish', function(e) {
			e.preventDefault();
			
			__loadQuestion(1);

		});

		function __getAnswer() {
			var _ans = $("input[name='answer']:checked").val();
			if(_ans) return _ans;

			return 0;
		}

		function __beforeEx() {
			$('#countStartExam').html('<li><h2 class="pt-4">' + 'ចាប់ផ្តើមប្រលង' + '</h2></li>');
			$('#beforeEx').remove();
			{{-- examination start --}}
			__doEx();
		}

		function __doEx() {
			$('#doEx').downCount({
				date: '12/20/2019 12:30:00',
				offset: +7
			}, function () {
				alert("No time");
				__finished();
			});

			$('#doEx').show();
		}

		function __finished() {
			window.location.href = '{{ route("frontend.quiz.result", $section->id) }}';
		}

		function __loadQuestion(_finished=0) {
			__loading($('#questionContent')); // loading

			$.ajax({
				type: 'GET',
				url: '{{ route("frontend.quiz.show", $section->id) }}',
				data: {order: _order, question: _question, answer: _answer},
				success: function(res) {
					if (res.finished == 1 || _finished == 1) {
						$('#questionContent').html('');
						alert("No question");
						__finished();
					}
					else {

						$('#questionContent').html(res.html);
						_answer = 0;
						_question = res.question;
					}
				}
			});
		}

		function __loading(el) {
			el.html($('#loading').html());
		}

	});
</script>
@endpush
