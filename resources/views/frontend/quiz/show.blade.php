<div class="card-header">
	សំណួរទី {{ $question->order }}/{{ $question->total }}
	{!! $question->finished_button !!}
</div>

<div class="card-body">
	<div class="row">
		<div class="col-sm-6">
			<p class="question-text"><span class="questionID">{{ $question->order }}.</span>
				{{ $question->text }}				
			</p>

			@if($question->image)
				<div class="question-text">
					<img class="img-fluid" src="{{ asset('img/upload/' . $question->section->id . '/' . $question->image) }}">
				</div>
			@else
				<div class="question-text">
					<img class="img-fluid" src="{{ asset('img/frontend/no_image.png') }}">
				</div>
			@endif
		</div>
		<div class="col-sm-6">
			<p class="question-text">ចម្លើយ៖</p>
			<div id="checkAnswer" class="checkAnswer">
				<div>
					@foreach($question->answers as $answer)
						<div class="form-group">
							<label>
								<input type="radio" value="{{ $answer->id }}" {{ (!is_null($question->answerUser) && $question->answerUser->answer_id == $answer->id ? 'checked' : '') }} class="option-input radio" name="answer" />
								{{ $answer->text }}
							</label>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>

<div class="card-footer">
	{!! $question->prev_next !!}
</div>