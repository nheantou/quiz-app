@extends('frontend.layouts.login')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.login_box_title'))

@section('content')
	<div class="row justify-content-center align-items-center">
		<div class="col col-sm-5 align-self-center formlogin">

			<div class="card">
				<div class="card-header">
                <i class="fa fa-unlock-alt"></i>
					<strong>
						@lang('labels.frontend.auth.login_box_title')
					</strong>
				</div>

				<div class="card-body">
					{{ html()->form('POST', route('frontend.auth.login.post'))->open() }}
						<div class="row">
							<div class="col">
                            <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                            </div>
                            {{ html()->email('email')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.frontend.email'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                            </div>
							</div>
						</div>

						<div class="row">
							<div class="col">
                            <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-lock"></i></span>
                            </div>
                            {{ html()->password('password')
										->class('form-control')
										->placeholder(__('validation.attributes.frontend.password'))
										->required() }}

                        </div>
						</div>
						</div>

						<div class="row">
							<div class="col">
								<div class="form-group clearfix">
									{{ form_submit(__('labels.frontend.auth.login_button'), 'btn btn-primary btn-block') }}
								</div>
							</div>
						</div>

					{{ html()->form()->close() }}

					<div class="row">
						<div class="col">
							<div class="text-center">
								{!! $socialiteLinks !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
