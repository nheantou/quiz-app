@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="icon-book-open icons"></i> @lang('labels.backend.question.mange')
                        <div class="card-header-actions">
                            <a href="{{ route('admin.question.create') }}" class="btn btn-primary btn-sm"><i class="fa icon-plus"></i>
                                @lang('buttons.new')
                            </a>
                        </div>
                        </div>
                        <div class="card-body">
                        <table class="table table-striped table-bordered datatable">
                            <thead>
                                <tr>
                                    <th>@lang('labels.backend.question.id')</th>
                                    <th>@lang('labels.backend.question.name')</th>
                                    <th>@lang('labels.backend.question.image')</th>
                                    <th>@lang('labels.backend.question.section')</th>
                                    <th>@lang('labels.backend.question.status')</th>
                                    <th>@lang('labels.backend.question.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($questions as $question)
                                <tr>
                                    <td>{{ $question->id }}</td>
                                    <td>{{ $question->text }}</td>
                                    @if($question->image)
                                        <td class="text-center w-25"><img class="img-fluid w-25" src="{{ asset('img/upload/' .$question->section->id.'/'.$question->image) }}"></td>
                                    @else
                                        <td class="text-center w-25"><img class="img-fluid w-25" src="{{ asset('img/backend/not-available.jpg') }}"></td>
                                    @endif
                                    <td>{{ $question->section->title }}</td>
                                    <td class="text-center">
                                        @if($question->active >0)
                                        <span class="badge badge-success">Active</span>
                                        @else
                                        <span class="badge badge-danger">Deactive</span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        {!! $question->action_buttons !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@push('after-scripts')

<script>
    $(function () {
        $('.btnEdit').click(function () {
            $.ajax({
                type: 'POST',
                url: $(this).data('remote'),
                success: function (res) {
                    $('textarea[name="txtquestion"]').val('some value');
                    $('#successModal').modal('show');
                }
            });
        });

        $('.btnDelete').click(function(){
        if(confirm("Are You Sure?")){
            $.post($(this).data('remote'),function(data){
            console.log(data);
        });
        }
        })
    });
</script>
@endpush
