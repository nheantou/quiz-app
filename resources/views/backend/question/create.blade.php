@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))
@push('after-styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
@endpush
@section('content')
<div class="animated fadeIn">
    <div class="row">
        <div class="col-sm-12">
            {{ html()->form('POST', route('admin.question.save'))->acceptsFiles()->open() }}
            <div class="card">
                <div class="card-header">
                    <i class="icon-book-open icons"></i> @lang('labels.backend.question.create')
                    {{-- <div class="card-header-actions">
                        <button data-toggle="modal" data-target="#successModal" class="btn btn-primary btn-sm"><i class="fa icon-plus"></i>
                            @lang('buttons.save')</button>
                    </div> --}}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <!-- /.row-->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="name">​ @lang('labels.backend.section.name') *</label>
                                            <select class="form-control js-example-basic" name="section">
                                                @foreach ($sections as $section)
                                                <option value="{{ $section->id }}">{{ $section->title }}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                        </div>
                            <!-- /.row-->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="name">​ @lang('labels.backend.question.name') *</label>
                                        <textarea class="form-control bg-light small" name="txtquestion" rows="4" ​="" required=""></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row-->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="ccnumber"> @lang('labels.backend.question.image')</label>
                                        <input type="file" class="form-control-file" name="fileImage">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <!-- /.1-->
                            <div class="row">
                                <div class="col-10">
                                    <div class="mb-1"> @lang('labels.backend.answer.1')​</div>
                                    <input class="form-control" name="answers[1]" type="text" required>
                                </div>
                                <div class="col-2">
                                    <div class="mb-2"> @lang('labels.backend.answer.right')</div>
                                    <label class="switch switch-label switch-pill switch-success">
                                        <input class="switch-input" type="radio" name="correct" value="1">
                                        <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                    </label>
                                </div>
                            </div>
                            <!-- /.2-->
                            <div class="row">
                                <div class="col-10">
                                    <div class="mb-1"> @lang('labels.backend.answer.2')​</div>
                                    <input class="form-control" name="answers[2]" type="text" required>
                                </div>
                                <div class="col-2">
                                <div class="mb-2"> @lang('labels.backend.answer.right') *</div>
                                    <label class="switch switch-label switch-pill switch-success">
                                        <input class="switch-input" type="radio" name="correct" value="2">
                                        <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                    </label>
                                </div>
                            </div>
                            <!-- /.3-->
                            <div class="row">
                                <div class="col-10">
                                    <div class="mb-1"> @lang('labels.backend.answer.3')​</div>
                                    <input class="form-control" name="answers[3]" type="text" required>
                                </div>
                                <div class="col-2">
                                <div class="mb-2"> @lang('labels.backend.answer.right')</div>
                                    <label class="switch switch-label switch-pill switch-success">
                                        <input class="switch-input" type="radio" name="correct" value="3">
                                        <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                    </label>
                                </div>
                            </div>
                            <!-- /.4-->
                            <div class="row">
                                <div class="col-10">
                                    <div class="mb-1"> @lang('labels.backend.answer.4')​</div>
                                    <input class="form-control" name="answers[4]" type="text" required>
                                </div>
                                <div class="col-2">
                                <div class="mb-2"> @lang('labels.backend.answer.right')</div>
                                    <label class="switch switch-label switch-pill switch-success">
                                        <input class="switch-input" type="radio" name="correct" value="4">
                                        <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="card-header-actions">
                        <a href="{{ route('admin.question.index') }}" class="btn btn-danger btn-sm"><i class="fa fa-times"></i>
                            @lang('buttons.cancel')
                        </a>
                        <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>
                            @lang('buttons.save')
                        </button>
                        <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>
                            @lang('buttons.savecontinue')
                        </button>
                    </div>
                </div>
            </div>
            {{ html()->form()->close() }} 
        </div>
    </div>
</div>
@endsection
@push('after-scripts')
    <script>
        $(function () {
            $('.js-example-basic').select2();
            $('.btnEdit').click(function () {
                $.ajax({
                    type: 'POST',
                    url: $(this).data('remote'),
                    success: function (res) {
                        $('textarea[name="txtquestion"]').val('some value');
                        $('#successModal').modal('show');
                    }
                });
            });

            $('.btnDelete').click(function(){
            if(confirm("Are You Sure?")){
                $.post($(this).data('remote'),function(data){
                console.log(data);
            });
            }
            })
        });
            // In your Javascript (external .js resource or <script> tag)
                $(document).ready(function() {
                
            });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
@endpush
