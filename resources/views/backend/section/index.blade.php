@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<div class="animated fadeIn">
            <!-- Start Insert Test -->
            <form id="formAdd" action="{{ route('admin.section.store') }}" method="POST">
            <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-success" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title">{{ __('labels.backend.section.create') }}</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">{{ __('labels.backend.section.name') }}</div>
                                    <input class="form-control" name="title" type="text" required>
                                </div>
                            </div>
                            <div class="row py-3">
                                <div class="col-10">
                                    <div class="mb-1">{{ __('labels.backend.section.during') }}</div>
                                    <input class="form-control" name="during" type="number" required>
                                </div>
                                <div class="col-2">
                                    <div class="mb-2">{{ __('labels.backend.section.status') }}</div>
                                    <label class="switch switch-label switch-pill switch-success">
                                        <input class="switch-input" type="radio" name="active" value="1">
                                        <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="icon-close icons"></i> {{ __('buttons.backend.close') }}</button>
                            <button class="btn btn-success btn-add ladda-button" type="submit"><i class="icon-check icons"></i> {{ __('buttons.backend.save') }}</button>
                        </div>
                    </div>
                <!-- /.modal-content-->
                </div>
                <!-- /.modal-dialog-->
            </div>
            @csrf
            </form>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="icon-note icons"></i> @lang('labels.backend.section.mange')
                        <div class="card-header-actions">
                            <button data-toggle="modal" data-target="#successModal" class="btn btn-primary btn-sm"><i class="fa icon-plus"></i>
                                @lang('buttons.new')</button>
                        </div>
                        </div>
                        <div class="card-body">
                        <table class="table table-striped table-bordered datatable">
                            <thead class="text-center">
                            <tr>
                                <th>@lang('labels.backend.section.id')</th>
                                <th>@lang('labels.backend.section.name')</th>
                                <th>@lang('labels.backend.section.during')</th>
                                <th>@lang('labels.backend.section.questions')</th>
                                <th>@lang('labels.backend.section.status')</th>
                                <th>@lang('labels.backend.section.action')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($sections as $section)
                            <tr>
                                <td>{{$section->id}}</td>
                                <td>{{$section->title}}</td>
                                <td>{{$section->during}}</td>
                                <td>{{$section->questions->count()}}</td>
                                <td class="text-center">
                                    @if($section->active > 0)
                                    <span class="badge badge-success">សកម្ម<span>
                                    @else
                                    <span class="badge badge-danger">អសកម្ម<span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    {!! $section->action_buttons !!}
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@push('after-scripts')

<script>
    $(function () {
        $('.btnEdit').click(function () {
            $.ajax({
                type: 'POST',
                url: $(this).data('remote'),
                success: function (data) {
                    $('input[name="txtsection"]').val(data['title']);
                    $('input[name="start_time"]').val(data['start_time']);
                    $('.btn-add').text(' កែប្រែ');
                    $('#successModal').modal('show');
                    console.log(data);
                }
            });
        });

        $('.btnDelete').click(function(){
        if(confirm("Are You Sure?")){
            $.post($(this).data('remote'),function(data){
            console.log(data);
        });
        }
        })
    });
</script>
@endpush
