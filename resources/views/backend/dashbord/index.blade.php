@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
    
<div class="row">
            <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-primary">
            <div class="card-body pb-0">
            <div class="text-value">9.823</div>
            <div>សិស្សសរុប</div>
            </div>
            <div class="chart-wrapper mt-3 mx-3" style="height:70px;"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
            <canvas class="chart chartjs-render-monitor" id="card-chart1" height="140" width="420" style="display: block; width: 210px; height: 70px;"></canvas>
            <div id="card-chart1-tooltip" class="chartjs-tooltip top bottom" style="opacity: 0; left: 227.558px; top: 139.722px;"><div class="tooltip-header"><div class="tooltip-header-item">July</div></div><div class="tooltip-body"><div class="tooltip-body-item"><span class="tooltip-body-item-color" style="background-color: rgb(0, 165, 224);"></span><span class="tooltip-body-item-label">My First dataset</span><span class="tooltip-body-item-value">40</span></div></div></div></div>
            </div>
            </div>

            <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-info">
            <div class="card-body pb-0">
            <div class="text-value">9.823</div>
            <div>សិស្សចូលប្រឡង</div>
            </div>
            <div class="chart-wrapper mt-3 mx-3" style="height:70px;"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
            <canvas class="chart chartjs-render-monitor" id="card-chart2" height="140" width="420" style="display: block; width: 210px; height: 70px;"></canvas>
            <div id="card-chart2-tooltip" class="chartjs-tooltip top bottom" style="opacity: 0; left: 56.5576px; top: 115.837px;"><div class="tooltip-header"><div class="tooltip-header-item">February</div></div><div class="tooltip-body"><div class="tooltip-body-item"><span class="tooltip-body-item-color" style="background-color: rgb(38, 203, 253);"></span><span class="tooltip-body-item-label">My First dataset</span><span class="tooltip-body-item-value">18</span></div></div></div></div>
            </div>
            </div>

            <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-warning">
            <div class="card-body pb-0">
            <div class="btn-group float-right">
            <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
            </div>
            </div>
            <div class="text-value">9.823</div>
            <div>សិស្សប្រឡងជាប់</div>
            </div>
            <div class="chart-wrapper mt-3" style="height:70px;"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
            <canvas class="chart chartjs-render-monitor" id="card-chart3" height="140" width="484" style="display: block; width: 242px; height: 70px;"></canvas>
            <div id="card-chart3-tooltip" class="chartjs-tooltip bottom top" style="opacity: 0; left: 41px; top: 101.3px;"><div class="tooltip-header"><div class="tooltip-header-item">February</div></div><div class="tooltip-body"><div class="tooltip-body-item"><span class="tooltip-body-item-color" style="background-color: rgba(230, 230, 230, 0.2);"></span><span class="tooltip-body-item-label">My First dataset</span><span class="tooltip-body-item-value">81</span></div></div></div></div>
            </div>
            </div>

            <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-danger">
            <div class="card-body pb-0">
            <div class="btn-group float-right">
            <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
            </div>
            </div>
            <div class="text-value">9.823</div>
            <div>សិស្សប្រឡងធ្លាក់</div>
            </div>
            <div class="chart-wrapper mt-3 mx-3" style="height:70px;"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
            <canvas class="chart chartjs-render-monitor" id="card-chart4" height="140" width="420" style="display: block; width: 210px; height: 70px;"></canvas>
            </div>
            </div>
            </div>

</div>
<div class="row">
    
    <div class="col-sm-12">
      <div class="card">
            <div class="card-body">
            <div class="row">
            <div class="col-sm-5">
            <h4 class="card-title mb-0">Report</h4>
            <div class="small text-muted">November 2017</div>
            </div>

            <div class="col-sm-7 d-none d-md-block">
            <button class="btn btn-primary float-right" type="button">
            <i class="icon-cloud-download"></i>
            </button>
            <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
            <label class="btn btn-outline-secondary">
            <input id="option1" type="radio" name="options" autocomplete="off"> Day
            </label>
            <label class="btn btn-outline-secondary active">
            <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
            </label>
            <label class="btn btn-outline-secondary">
            <input id="option3" type="radio" name="options" autocomplete="off"> Year
            </label>
            </div>
            </div>

            </div>

            <div class="chart-wrapper" style="height:300px;margin-top:40px;"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
            <canvas class="chart chartjs-render-monitor" id="main-chart" height="600" width="2048" style="display: block; width: 1024px; height: 300px;"></canvas>
            </div>
            </div>
            <div class="card-footer">
                        <div class="row text-center">
                        <div class="col-sm-12 col-md mb-sm-2 mb-0">
                            <button class="btn btn-pill btn-block btn-primary" type="button">Primary</button>
                        </div>
                        <div class="col-sm-12 col-md mb-sm-2 mb-0">
                                <button class="btn btn-pill btn-block btn-primary" type="button">Primary</button>
                        </div>
                        <div class="col-sm-12 col-md mb-sm-2 mb-0">
                                <button class="btn btn-pill btn-block btn-primary" type="button">Primary</button>
                        </div>
                        <div class="col-sm-12 col-md mb-sm-2 mb-0">
                                <button class="btn btn-pill btn-block btn-primary" type="button">Primary</button>
                        </div>
                        <div class="col-sm-12 col-md mb-sm-2 mb-0">
                                <button class="btn btn-pill btn-block btn-primary" type="button">Primary</button>
                        </div>
            </div>
            </div>
            </div>
    </div>

</div>

@endsection
