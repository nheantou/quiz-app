@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<div class="animated fadeIn">
            <!-- Start Insert Test -->
            {{ html()->form('POST', route('admin.question.store', $section->id))->acceptsFiles()->open() }}
            <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-success" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title">បង្កើតសំណួរប្រឡង</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col">
                                    <div class="my-2">សំណួរ</div>
                                    <textarea class="form-control bg-light small" name="txtquestion" rows="4"​ required></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="my-2">ជ្រើសរើសរូបភាព (ប្រសិនបើមាន)</div>
                                    <div class="form-group">
                                        <input type="file" class="form-control-file" name="fileImage">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-10">
                                    <div class="mb-1">ចម្លើយទី១</div>
                                    <input class="form-control" name="answers[1]" type="text" required>
                                </div>
                                <div class="col-2">
                                <div class="mb-2">ត្រឹមត្រូវ</div>
                                    <label class="switch switch-label switch-pill switch-success">
                                        <input class="switch-input" type="radio" name="correct" value="1">
                                        <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-10">
                                    <div class="mb-1">ចម្លើយទី២</div>
                                    <input class="form-control" name="answers[2]" type="text" required>
                                </div>
                                <div class="col-2">
                                <div class="mb-2">ត្រឹមត្រូវ</div>
                                    <label class="switch switch-label switch-pill switch-success">
                                        <input class="switch-input" type="radio" name="correct" value="2">
                                        <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-10">
                                    <div class="mb-1">ចម្លើយទី៣</div>
                                    <input class="form-control" name="answers[3]" type="text" required>
                                </div>
                                <div class="col-2">
                                <div class="mb-2">ត្រឹមត្រូវ</div>
                                    <label class="switch switch-label switch-pill switch-success">
                                        <input class="switch-input" type="radio" name="correct" value="3">
                                        <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-10">
                                    <div class="mb-1">ចម្លើយទី៤</div>
                                    <input class="form-control" name="answers[4]" type="text" required>
                                </div>
                                <div class="col-2">
                                <div class="mb-2">ត្រឹមត្រូវ</div>
                                    <label class="switch switch-label switch-pill switch-success">
                                        <input class="switch-input" type="radio" name="correct" value="4">
                                        <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="icon-close icons"></i> បិទ</button>
                            <button class="btn btn-success btn-ladda ladda-button" type="submit"><i class="icon-check icons"></i> រក្សាទុក</button>
                        </div>
                    </div>
                <!-- /.modal-content-->
                </div>
                <!-- /.modal-dialog-->
            </div>
            {{ html()->form()->close() }}
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="icon-book-open icons"></i> @lang('labels.backend.question.mange')
                        <div class="card-header-actions">
                            <button data-toggle="modal" data-target="#successModal" class="btn btn-primary btn-sm"><i class="fa icon-plus"></i>
                                @lang('buttons.new')</button>
                        </div>
                        </div>
                        <div class="card-body">
                        <table class="table table-striped table-bordered datatable">
                            <thead>
                            <tr>
                                <th>@lang('labels.backend.question.no')</th>
                                <th>@lang('labels.backend.question.name')</th>
                                <th>@lang('labels.backend.question.image')</th>
                                <th>@lang('labels.backend.question.status')</th>
                                <th>@lang('labels.backend.question.order')</th>
                                <th>@lang('labels.backend.question.action')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($section->questions as $question)
                    <tr>
                        <td>{{ $question->id }}</td>
                        <td>{{ $question->text }}</td>
                        @if($question->image)
                            <td class="text-center w-25"><img class="img-fluid w-25" src="{{ asset('img/upload/' .$question->section->id.'/'.$question->image) }}"></td>
                        @else
                            <td class="text-center w-25"><img class="img-fluid w-25" src="{{ asset('img/backend/not-available.jpg') }}"></td>
                        @endif
                        <td class="text-center">
                            @if($question->active >0)
                            <span class="badge badge-success">Active</span>
                            @else
                            <span class="badge badge-danger">Deactive</span>
                            @endif
                        </td>
                        <td>{{ $question->order }}</td>
                        <td class="text-center">
                            {!! $question->action_buttons !!}
                        </td>
                    </tr>
                    @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@push('after-scripts')

<script>
    $(function () {
        $('.btnEdit').click(function () {
            $.ajax({
                type: 'POST',
                url: $(this).data('remote'),
                success: function (res) {
                    $('textarea[name="txtquestion"]').val('some value');
                    $('#successModal').modal('show');
                }
            });
        });

        $('.btnDelete').click(function(){
        if(confirm("Are You Sure?")){
            $.post($(this).data('remote'),function(data){
            console.log(data);
        });
        }
        })
    });
</script>
@endpush
