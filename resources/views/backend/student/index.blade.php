@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<div class="animated fadeIn">
            <!-- Start Insert Test -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="icon-note icons"></i> @lang('labels.backend.student.mange')
                        <div class="card-header-actions">
                            <a href="{{ route('admin.auth.user.create') }}" class="btn btn-primary btn-sm"><i class="fa icon-plus"></i>
                                @lang('labels.backend.student.create')
                            </a>
                        </div>
                        </div>
                        <div class="card-body">
                        <table class="table table-striped table-bordered datatable">
                            <thead class="text-center">
                            <tr>
                                <th>@lang('labels.backend.student.id')</th>
                                <th>@lang('labels.backend.student.first_name')</th>
                                <th>@lang('labels.backend.student.last_name')</th>
                                <th>@lang('labels.backend.student.email')</th>
                                <th>@lang('labels.backend.student.avatar_location')</th>
                                <th>@lang('labels.backend.student.action')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data as $student)
                            @if($student->roles_label == 'User')
                            <tr>
                                <td>{{$student->id}}</td>
                                <td>{{$student->first_name}}</td>
                                <td>{{$student->last_name}}</td>
                                <td>{{$student->email}}</td>
                                <td>{{$student->avatar_location}}</td>
                                <td class="text-center">
                                    {!! $student->action_buttons !!}
                                </td>
                            </tr>
                            @endif
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@push('after-scripts')

<script>
    $(function () {
        $('.btnEdit').click(function () {
            $.ajax({
                type: 'POST',
                url: $(this).data('remote'),
                success: function (data) {
                    $('input[name="txtsection"]').val(data['title']);
                    $('input[name="start_time"]').val(data['start_time']);
                    $('.btn-add').text(' កែប្រែ');
                    $('#successModal').modal('show');
                    console.log(data);
                }
            });
        });

        $('.btnDelete').click(function(){
        if(confirm("Are You Sure?")){
            $.post($(this).data('remote'),function(data){
            console.log(data);
        });
        }
        })
    });
</script>
@endpush
