<?php

return [
	/*
	|--------------------------------------------------------------------------
	| Buttons Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used in buttons throughout the system.
	| Regardless where it is placed, a button can be listed here so it is easily
	| found in a intuitive way.
	|
	*/

	'test'          => 'ចូលតេស្តប្រឡង',
	'result'		=> 'បង្ហាញលទ្ធផល',
	'finished'		=> 'បញ្ចប់ការប្រឡង',
	'exit'			=> 'ចាកចេញ',
	'new'			=> 'បង្កើតថ្មី',
	'save'			=> 'រក្សាទុក',
	'savecontinue'  => "រក្សាទុកបន្ដ",
	'cancel'		=> 'បោះបង់',
	'questions'		=> [
		'prev'		=> 'សំណួរមុន',
		'next'		=> 'សំណួរបន្ទាប់',
		'last'		=> 'សំណួរចុងក្រោយ'
	],

	'backend' => [
		'close'          => 'បិទ',
		'save'          => 'រក្សាទុក',
		'access' => [
			'users' => [
				'activate' => 'Activate',
				'change_password' => 'Change Password',
				'clear_session' => 'Clear Session',
				'confirm' => 'Confirm',
				'deactivate' => 'Deactivate',
				'delete_permanently' => 'Delete Permanently',
				'login_as' => 'Login As :user',
				'resend_email' => 'Resend Confirmation E-mail',
				'restore_user' => 'Restore User',
				'unconfirm' => 'Un-confirm',
				'unlink' => 'Unlink',
			],
		],
	],

	'emails' => [
		'auth' => [
			'confirm_account' => 'Confirm Account',
			'reset_password' => 'Reset Password',
		],
	],

	'general' => [
		'cancel' => 'Cancel',
		'continue' => 'Continue',

		'crud' => [
			'create' => 'Create',
			'delete' => 'Delete',
			'edit' => 'Edit',
			'update' => 'Update',
			'view' => 'View',
		],

		'save' => 'Save',
		'view' => 'View',
	],
];
