<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'លិខិតសម្គាល់ទាំងនេះមិនត្រូវគ្នានឹងកំណត់ត្រារបស់យើងទេ។',
    'general_error' => 'អ្នកពុំសិទ្ធក្នុងចូលក្នុងប្រពន័នេះទេ.',
    'password_rules' => 'Your password must be more than 8 characters long, should contain at least 1 uppercase, 1 lowercase and 1 number.',
    'password_used' => 'You can not set a password that you have previously used.',
    'socialite' => [
        'unacceptable' => ':provider is not an acceptable login type.',
    ],
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'unknown' => 'An unknown error occurred',
];
