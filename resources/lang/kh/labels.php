<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all' => 'All',
        'yes' => 'Yes',
        'no' => 'No',
        'copyright' => 'រក្សាសិទ្ធគ្រប់យ៉ាងដោយ',
        'creator' => 'ប្រព័ន្ធនេះបង្កើតដោយ',
        'custom' => 'Custom',
        'actions' => 'Actions',
        'active' => 'Active',
        'buttons' => [
            'save' => 'Save',
            'update' => 'Update',
        ],
        'hide' => 'Hide',
        'inactive' => 'Inactive',
        'none' => 'None',
        'show' => 'Show',
        'toggle_navigation' => 'Toggle Navigation',
        'create_new' => 'Create New',
        'toolbar_btn_groups' => 'Toolbar with button groups',
        'more' => 'More',
    ],
    
    'backend' => [
        'section' =>[
            'create' => "បង្កើតការប្រឡង",
            'name' => "ឈ្មោះប្រឡង",
            'status' => "ស្ថានភាព",
            'during' => "រយះពេល/នាទី",
            'time' => "គិតជា",
            'id' => "លេខរៀង",
            'questions' =>"ចំនួនសំណួរ",
            'mange' => "គ្របគ្រងវិញាសារប្រឡង",
            'action' => "សកម្មភាព",
        ],
        'question' =>[
            'create' => "បង្កើតសំណួរ",
            'name' => "សំណួរ",
            'order' => "លំដាប់ថ្នាក់",
            'image' => "រូបភាព",
            'status' => "ស្ថានភាព",
            'id' => "លេខរៀង",
            'mange' => "គ្របគ្រងសំណួរប្រឡង",
            'action' => "សកម្មភាព",
            'section' =>"ឈ្មោះប្រឡង"
        ],
        'answer' =>[
            'create' => "បង្កើតចម្លើយ",
            '1' => "ចំម្លើយទី១",
            '2'=> "ចំម្លើយទី២",
            '3'=> "ចំម្លើយទី៣",
            '4'=> "ចំម្លើយទី៤",
            '5'=> "ចំម្លើយទី៥",
            'right' => "ត្រឹមត្រូវ",
            'image' => "រូបភាព",
            'status' => "ស្ថានភាព",
            'id' => "លេខរៀង",
            'mange' => "គ្របគ្រងវិញាសារប្រឡង",
            'action' => "សកម្មភាព",
        ],
        'student' =>[
            'create' => "ចុះឈ្មោះសិស្ស",
            'first_name' => "នាមខ្លួន",
            'last_name' => "នាមត្រកូល",
            'email' => "អ៊ីម៉ែល",
            'avatar_location' => "រូបថត",
            'id' => "លេខរៀង",
            'questions' =>"ចំនួនសំណួរ",
            'mange' => "គ្របគ្រងសិស្ស",
            'action' => "សកម្មភាព",
        ],
        'access' => [
            'roles' => [
                'create' => 'Create Role',
                'edit' => 'Edit Role',
                'management' => 'Role Management',

                'table' => [
                    'number_of_users' => 'Number of Users',
                    'permissions' => 'Permissions',
                    'role' => 'Role',
                    'sort' => 'Sort',
                    'total' => 'role total|roles total',
                ],
            ],
            
            'users' => [
                'active' => 'Active Users',
                'all_permissions' => 'All Permissions',
                'change_password' => 'Change Password',
                'change_password_for' => 'Change Password for :user',
                'create' => 'Create User',
                'deactivated' => 'Deactivated Users',
                'deleted' => 'Deleted Users',
                'edit' => 'Edit User',
                'management' => 'User Management',
                'no_permissions' => 'No Permissions',
                'no_roles' => 'No Roles to set.',
                'permissions' => 'Permissions',
                'user_actions' => 'User Actions',

                'table' => [
                    'confirmed' => 'Confirmed',
                    'created' => 'Created',
                    'email' => 'E-mail',
                    'id' => 'ID',
                    'last_updated' => 'Last Updated',
                    'name' => 'Name',
                    'first_name' => 'First Name',
                    'last_name' => 'Last Name',
                    'no_deactivated' => 'No Deactivated Users',
                    'no_deleted' => 'No Deleted Users',
                    'other_permissions' => 'Other Permissions',
                    'permissions' => 'Permissions',
                    'abilities' => 'Abilities',
                    'roles' => 'Roles',
                    'social' => 'Social',
                    'total' => 'user total|users total',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Overview',
                        'history' => 'History',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar' => 'Avatar',
                            'confirmed' => 'Confirmed',
                            'created_at' => 'Created At',
                            'deleted_at' => 'Deleted At',
                            'email' => 'E-mail',
                            'last_login_at' => 'Last Login At',
                            'last_login_ip' => 'Last Login IP',
                            'last_updated' => 'Last Updated',
                            'name' => 'Name',
                            'first_name' => 'First Name',
                            'last_name' => 'Last Name',
                            'status' => 'Status',
                            'timezone' => 'Timezone',
                        ],
                    ],
                ],

                'view' => 'View User',
            ],
        ],
    ],

    'frontend' => [
        'auth' => [
            'login_box_title' => 'Login',
            'login_button' => 'Login',
            'login_with' => 'Login with :social_media',
            'register_box_title' => 'Register',
            'register_button' => 'Register',
            'remember_me' => 'Remember Me',
        ],

        'contact' => [
            'box_title' => 'Contact Us',
            'button' => 'Send Information',
        ],

        'passwords' => [
            'expired_password_box_title' => 'Your password has expired.',
            'forgot_password' => 'Forgot Your Password?',
            'reset_password_box_title' => 'Reset Password',
            'reset_password_button' => 'Reset Password',
            'update_password_button' => 'Update Password',
            'send_password_reset_link_button' => 'Send Password Reset Link',
        ],

        'user' => [
            'passwords' => [
                'change' => 'Change Password',
            ],

            'profile' => [
                'avatar' => 'Avatar',
                'created_at' => 'Created At',
                'edit_information' => 'Edit Information',
                'email' => 'E-mail',
                'last_updated' => 'Last Updated',
                'name' => 'Name',
                'first_name' => 'First Name',
                'last_name' => 'Last Name',
                'update_information' => 'Update Information',
            ],
        ],
    ],
];
